const menuTrigger = document.getElementById("menu-trigger")
const closeMenu = document.getElementById("close-menu")
const navigation = document.getElementById("right-navigation")
menuTrigger.addEventListener("click", function(){
    closeMenu.classList.toggle("show-background-navigation")
    navigation.classList.toggle("show-navigation")
});

closeMenu.addEventListener("click", function(){
    closeMenu.classList.remove("show-background-navigation")
    navigation.classList.remove("show-navigation")
});
