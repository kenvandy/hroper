[...document.querySelectorAll('.category-item')]
    .forEach(categoryItem => {
        categoryItem.addEventListener('click',(event) => {
            var target = event.target
            var parent = target.parentElement
            if(target.tagName !== "A")
                parent = parent.parentElement
            if(!parent.classList.contains("selected"))
                removeSelected()
                
            parent.classList.toggle("selected")
        }); 
    })

function removeSelected() {
    [...document.querySelectorAll('.category')]
        .forEach(category => {
            category.classList.remove("selected")
        })
}
// order area avigation on desktop or tablet
const areaDeskTrigger = document.getElementById("area--desk-trigger")
const bgAreaDeskCloseMenu = document.getElementById("bg-close-area-box")
const areaDeskNavigation = document.getElementById("area-list-box")
const areaDeskResult = document.getElementById("area-desk-result")

areaDeskTrigger.addEventListener("click", function(){
    bgAreaDeskCloseMenu.classList.toggle("show-bg-close-area")
    areaDeskNavigation.classList.toggle("show-list-area")
});

bgAreaDeskCloseMenu.addEventListener("click", function(){
    bgAreaDeskCloseMenu.classList.remove("show-bg-close-area")
    areaDeskNavigation.classList.remove("show-list-area")
});

[...document.querySelectorAll('.area-box-item')]
    .forEach(areaItem => {
        areaItem.addEventListener('click',(event) => {
            var target = event.target
            console.log(target.innerHTML)
            areaResult.innerHTML = target.innerHTML
            areaDeskResult.innerHTML = target.innerHTML
            bgAreaDeskCloseMenu.classList.remove("show-bg-close-area")
            areaDeskNavigation.classList.remove("show-list-area")
        }); 
    })





// order area avigation on Phone
const areaTrigger = document.getElementById("area-trigger")
const bgAreaCloseMenu = document.getElementById("bg-area-close-menu")
const areaNavigation = document.getElementById("area-navigation--container")
const areaResult = document.getElementById("area-result")

areaTrigger.addEventListener("click", function(){
    bgAreaCloseMenu.classList.toggle("background-sub-menu--show")
    areaNavigation.classList.toggle("submenu-box--show")
});

bgAreaCloseMenu.addEventListener("click", function(){
    bgAreaCloseMenu.classList.remove("background-sub-menu--show")
    areaNavigation.classList.remove("submenu-box--show")
});

[...document.querySelectorAll('.area--item')]
    .forEach(areaItem => {
        areaItem.addEventListener('click',(event) => {
            var target = event.target
            console.log(target.innerHTML)
            areaResult.innerHTML = target.innerHTML
            areaDeskResult.innerHTML = target.innerHTML
            bgAreaCloseMenu.classList.remove("background-sub-menu--show")
            areaNavigation.classList.remove("submenu-box--show")
        }); 
    })

// order menu
const orderMenuTrigger = document.getElementById("order-menu-trigger")
const bgOrderMenuCloseMenu = document.getElementById("bg-order-menu-close-menu")
const orderMenuNavigation = document.getElementById("order-menu--container")

orderMenuTrigger.addEventListener("click", function(){
    bgOrderMenuCloseMenu.classList.toggle("background-sub-menu--show")
    orderMenuNavigation.classList.toggle("submenu-box--show")
});

bgOrderMenuCloseMenu.addEventListener("click", function(){
    bgOrderMenuCloseMenu.classList.remove("background-sub-menu--show")
    orderMenuNavigation.classList.remove("submenu-box--show")
});

[...document.querySelectorAll('.order-menu--item')]
    .forEach(orderMenuItem => {
        orderMenuItem.addEventListener('click',(event) => {
            var target = event.target
            var parent = target.parentElement
            if(!parent.classList.contains("sub-menu-box-selected"))
                removeSelected()
                
            parent.classList.toggle("sub-menu-box-selected")
        }); 
    })

function removeSelected() {
    [...document.querySelectorAll('.sub-menu-box-item')]
        .forEach(category => {
            category.classList.remove("sub-menu-box-selected")
        })
}


window.onresize = function(event) {
    initSetbodyNavigationHeight()
};

var UID = {
	_current: 0,
	getNew: function(){
		this._current++;
		return this._current;
	}
};

HTMLElement.prototype.pseudoStyle = function(element,prop,value){
	var _this = this;
	var _sheetId = "pseudoStyles";
	var _head = document.head || document.getElementsByTagName('head')[0];
	var _sheet = document.getElementById(_sheetId) || document.createElement('style');
	_sheet.id = _sheetId;
	var className = "pseudoStyle" + UID.getNew();
	
	_this.className +=  " "+className; 
	
	_sheet.innerHTML += " ."+className+":"+element+"{"+prop+":"+value+"}";
	_head.appendChild(_sheet);
	return this;
};

function initSetbodyNavigationHeight() {
    [...document.querySelectorAll('.body--navigation-item')]
    .forEach(bodyNavigationItem => {
        var height = bodyNavigationItem.offsetHeight
        console.log(height)
        bodyNavigationItem.pseudoStyle("after","border-top-width",height/2+"px !important")
        .pseudoStyle("after","border-bottom-width",height/2+"px !important")
        // .pseudoStyle("after","border-left-width",height/2+"px !important")
        // .pseudoStyle("after","right",-height/2+"px")
        .pseudoStyle("before","border-top-width",height/2+"px !important")
        .pseudoStyle("before","border-bottom-width",height/2+"px !important")
        // .pseudoStyle("before","border-left-width",height/2+"px !important")
    })
}
  
initSetbodyNavigationHeight();