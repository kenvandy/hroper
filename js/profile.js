const subMenuTrigger = document.getElementById("sub-menu-trigger")
const bgCloseMenu = document.getElementById("bg-close-menu")
const subNavigation = document.getElementById("sub-menu-box--container")
const closeMenuProfile = document.getElementById("close-menu-profile")
subMenuTrigger.addEventListener("click", function(){
    bgCloseMenu.classList.toggle("background-sub-menu--show")
    subNavigation.classList.toggle("submenu-box--show")
});

bgCloseMenu.addEventListener("click", function(){
    bgCloseMenu.classList.remove("background-sub-menu--show")
    subNavigation.classList.remove("submenu-box--show")
});

closeMenuProfile.addEventListener("click", function(){
    bgCloseMenu.classList.remove("background-sub-menu--show")
    subNavigation.classList.remove("submenu-box--show")
});
