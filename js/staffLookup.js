var advanceInfo = document.getElementById("advanceInfo")
var advanceToggle = document.getElementById("toggleImg")
var togglePolygon = document.getElementById("togglePolygonImg")

advanceToggle.src='./assets/icons/icon-union.svg'
togglePolygon.src='./assets/icons/ic-polygon-white.svg'

toggleBtn()
function toggleBtn() {
  if (advanceInfo.style.display === "none") {
    advanceInfo.style.display = "grid"
    advanceToggle.src="./assets/icons/icon-subtract.svg"
    togglePolygon.src='./assets/icons/ic-polygon-whiteup.svg'
  } else {
    advanceInfo.style.display = "none"
    advanceToggle.src="./assets/icons/icon-union.svg"
    togglePolygon.src='./assets/icons/ic-polygon-white.svg'
  }
}