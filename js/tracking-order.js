var trackingMenu = document.getElementById("trackingMenu")
var togglePolygon = document.getElementById("togglePolygonImg")

togglePolygon.src='./assets/icons/ic-polygon-white.svg'

toggleBtn()
function toggleBtn() {
  if (trackingMenu.style.display === "none") {
    trackingMenu.style.display = "flex"
    togglePolygon.src='./assets/icons/ic-polygon-whiteup.svg'
  } else {
    trackingMenu.style.display = "none"
    togglePolygon.src='./assets/icons/ic-polygon-white.svg'
  }
}